<?php
/**
 * @package     Igorludgero_Correios
 * @author      Igor Ludgero Miura - https://www.igorludgero.com/ - igor@igorludgero.com
 * @copyright   Igor Ludgero Miura - https://www.igorludgero.com/ - igor@igorludgero.com
 * @license     https://opensource.org/licenses/AFL-3.0  Academic Free License 3.0 | Open Source Initiative
 */

namespace Igorludgero\Correios\Controller\Adminhtml\Cotacoes;

use Magento\Backend\App\Action\Context;
use Igorludgero\Correios\Model\CotacoesRepository;
use Magento\Framework\Controller\ResultFactory;

class ClearDb extends \Magento\Backend\App\Action
{

    protected $cotacoesRepository;

    public function __construct(Context $context, CotacoesRepository $cotacoesRepository)
    {
        $this->cotacoesRepository = $cotacoesRepository;
        parent::__construct($context);
    }

    public function execute()
    {
        $error = 0;

        $collection = $this->cotacoesRepository->getCollection();

        foreach ($collection as $cotacao){
            if($cotacao->delete()==false)
                $error++;
        }

        if($error==0)
            $this->messageManager->addSuccessMessage(__('The database of postcode tracks was succesfully cleared.'));
        else
            $this->messageManager->addErrorMessage(__("An error occurred when tried to clear the postcode database."));

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/');
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Igorludgero_Correios::correios_menuoption1');
    }

}