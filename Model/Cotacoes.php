<?php
/**
 * @package     Igorludgero_Correios
 * @author      Igor Ludgero Miura - https://www.igorludgero.com/ - igor@igorludgero.com
 * @copyright   Igor Ludgero Miura - https://www.igorludgero.com/ - igor@igorludgero.com
 * @license     https://opensource.org/licenses/AFL-3.0  Academic Free License 3.0 | Open Source Initiative
 */

namespace Igorludgero\Correios\Model;

use Magento\Framework\Model\AbstractModel;

class Cotacoes extends AbstractModel
{

    public function __construct( \Magento\Framework\Model\Context $context,
                                 \Magento\Framework\Registry $registry,
                                 array $data = [])
    {
        parent::__construct($context, $registry, null, null, $data);
    }

    protected function _construct()
    {
        $this->_init('Igorludgero\Correios\Model\ResourceModel\Cotacoes');
    }

}