<?php

/**
 * @package     Igorludgero_Correios
 * @author      Igor Ludgero Miura - https://www.igorludgero.com/ - igor@igorludgero.com
 * @copyright   Igor Ludgero Miura - https://www.igorludgero.com/ - igor@igorludgero.com
 * @license     https://opensource.org/licenses/AFL-3.0  Academic Free License 3.0 | Open Source Initiative
 */

namespace Igorludgero\Correios\Api;

interface CotacoesInterface
{

    /**
     * Get cotacao from cotacao id.
     * @param $id
     * @return mixed
     */
    public function getById($id);

    /**
     * Get cotacoes from postcode.
     * @param $postcode
     * @return mixed
     */
    public function getFromPostcode($postcode);

    /**
     * Get collection.
     * @return mixed
     */
    public function getCollection();

    /**
     * Save cotacao model.
     * @param CotacoesInterface $model
     * @return mixed
     */
    public function save(CotacoesInterface $model);

    /**
     * Delete cotacao model.
     * @param CotacoesInterface $model
     * @return mixed
     */
    public function delete(CotacoesInterface $model);

    /**
     * Populate database with postcode tracks.
     * @return mixed
     */
    public function populate();

}